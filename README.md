## Conky Skeleton Wallpapers

A wallpapers collection for [Conky-Skeleton](https://gitlab.com/thegreatyellow67/conky-skeleton)

---

### Wallpapers:

## Black
<img src="https://i.imgur.com/shs0Wut.png">
<img src="https://i.imgur.com/XKTijCI.png">
<img src="https://i.imgur.com/9v5k9c8.png">
<img src="https://i.imgur.com/eVb7nxL.png">
<img src="https://i.imgur.com/00KRaof.png">

## Bluegrey
<img src="https://i.imgur.com/jO8eDVg.png">
<img src="https://i.imgur.com/ZrBkuME.png">
<img src="https://i.imgur.com/3ZMuOFn.png">
<img src="https://i.imgur.com/I1TNDyf.png">
<img src="https://i.imgur.com/tVP9AWZ.png">

## Blue
<img src="https://i.imgur.com/MT3TVAR.png">
<img src="https://i.imgur.com/iyLBpxw.png">
<img src="https://i.imgur.com/lGSCoAZ.png">
<img src="https://i.imgur.com/IU8ZgLm.png">
<img src="https://i.imgur.com/GG5qZsB.png">

## Brown
<img src="https://i.imgur.com/GIxeOoQ.png">
<img src="https://i.imgur.com/FW0RU0g.png">
<img src="https://i.imgur.com/xbsntH8.png">
<img src="https://i.imgur.com/RLTUdLz.png">
<img src="https://i.imgur.com/86Iyv94.png">

## Cyan
<img src="https://i.imgur.com/B7eEnwW.png">
<img src="https://i.imgur.com/exYAVPK.png">
<img src="https://i.imgur.com/LA9GUEM.png">
<img src="https://i.imgur.com/LIcEgvN.png">
<img src="https://i.imgur.com/p11sBbI.png">

## Green
<img src="https://i.imgur.com/PznxFxs.png">
<img src="https://i.imgur.com/ZEVVKCX.png">
<img src="https://i.imgur.com/ikLgXaY.png">
<img src="https://i.imgur.com/UQelm83.png">
<img src="https://i.imgur.com/1FD7gqJ.png">

## Grey
<img src="https://i.imgur.com/h6fd2JG.png">
<img src="https://i.imgur.com/cc3mL1K.png">
<img src="https://i.imgur.com/pqPxgnK.png">
<img src="https://i.imgur.com/3xBu0fW.png">
<img src="https://i.imgur.com/Ap3qQnU.png">

## Magenta
<img src="https://i.imgur.com/TRKFm1Z.png">
<img src="https://i.imgur.com/hoIhHsM.png">
<img src="https://i.imgur.com/s9PPR4z.png">
<img src="https://i.imgur.com/lUEGGtk.png">
<img src="https://i.imgur.com/ABNozid.png">

## Orange
<img src="https://i.imgur.com/mxFd83u.png">
<img src="https://i.imgur.com/OxCWm9G.png">
<img src="https://i.imgur.com/p6Wllih.png">
<img src="https://i.imgur.com/e7ba1an.png">
<img src="https://i.imgur.com/4jk3IMz.png">

## Red
<img src="https://i.imgur.com/fFffDXa.png">
<img src="https://i.imgur.com/qjtljLl.png">
<img src="https://i.imgur.com/LvFN1tJ.png">
<img src="https://i.imgur.com/lHCWTWB.png">
<img src="https://i.imgur.com/D9OtGg5.png">

## Teal
<img src="https://i.imgur.com/ZCvFBDS.png">
<img src="https://i.imgur.com/alM4y9g.png">
<img src="https://i.imgur.com/Rzoss82.png">
<img src="https://i.imgur.com/sv73r9p.png">
<img src="https://i.imgur.com/xbPszai.png">

## Violet
<img src="https://i.imgur.com/oxNM7g0.png">
<img src="https://i.imgur.com/IBvwC6a.png">
<img src="https://i.imgur.com/WAjAoVj.png">
<img src="https://i.imgur.com/zJAWWXY.png">
<img src="https://i.imgur.com/oyaqltL.png">

## Yellow
<img src="https://i.imgur.com/bt5lgw5.png">
<img src="https://i.imgur.com/eSDmF3y.png">
<img src="https://i.imgur.com/0q1ETIj.png">
<img src="https://i.imgur.com/7Nb5i9X.png">
<img src="https://i.imgur.com/7ADqqH3.png">
